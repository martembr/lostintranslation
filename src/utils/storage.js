// adds value to session storage with given key
export const setStorage = (key, value) => {
    sessionStorage.setItem(key, JSON.stringify(value));
}

// returns value stored in session storage with given key 
export const getStorage = (key) => {
    const value = sessionStorage.getItem(key);
    return JSON.parse(value);
}

// removes value associated with key from session storage
export const clearStorage = (key) => {
    sessionStorage.removeItem(key);
}
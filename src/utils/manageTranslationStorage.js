import { getStorage, setStorage } from './storage';

const MAX_NUMBER_OF_TRANSLATIONS = 10;

export const addTranslation = (text) => {
    let session = getStorage('session_info');
    if (!session) return; 
    if (session.translations.length === MAX_NUMBER_OF_TRANSLATIONS) {
        //remove first/oldest translations
        session.translations.splice(0,1);
    }
    // add new translations
    session.translations.push(text);
    // update in storage 
    setStorage('session_info', session);
}

export const getTranslations = () => {
    let session = getStorage('session_info');
    return !session ? [] : session.translations;
}

export const clearTranslations = () => {
    let session = getStorage('session_info');
    if (!session) return; 
    // set translations to empty array
    session.translations = [];
    // update in storage 
    setStorage('session_info', session);
}
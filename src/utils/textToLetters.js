export const getLettersFromText = (text) => {
    // split text into list of lowercase letters
    const inputList = text.toLowerCase().split("");
    // filter out non letters
    const letterList = inputList.filter( letter => { return /[a-z]/.test(letter)} );
    // return list of letters in range a-z
    return letterList;
}
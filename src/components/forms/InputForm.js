import React, { useState }  from 'react';
import '../styles/inputForm.css'

const InputForm = ( {placeholderString, click, buttonText }) => {

    const [ inputValue, setInputValue ] = useState('');

    const onSubmitted = (ev) => {
        // get entered text from input value
        let textEntered = inputValue;
        // reset input value
        setInputValue('');
        // send entered text to click function
        click(textEntered);
    }

    const onInputChanged = (ev) => {
        setInputValue(ev.target.value);
    }

    return (
        
            <form onSubmit = {(e) => e.preventDefault() }>
                <div className="wrapper">
                    <input type="text" placeholder={ placeholderString }
                    onChange={ onInputChanged } value={inputValue}/>
                    <button type="button" onClick = { onSubmitted }> { buttonText }</button>
                </div>
            </form>
        
    )

};

export default InputForm;
import React  from 'react';
import TranslationDisplay from './TranslationDisplay';
import '../styles/translationlistdisplay.css';

const TranslationListDisplay = ({translations}) => {

    // map to list of translationdisplays
    let index = 0;
    const displayList = translations.map( 
        textToTranslate => <li key={index++}><TranslationDisplay text={ textToTranslate } /></li>
    );

    return (    
        <div className="container">
            <ul className="translation-list">{ displayList }</ul>
        </div>
    )
};

export default TranslationListDisplay;
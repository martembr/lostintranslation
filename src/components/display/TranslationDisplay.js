import React  from 'react';
import { getLettersFromText } from '../../utils/textToLetters';
import '../styles/translationdisplay.css';

const TranslationDisplay = (props) => {

    const imageBaseUrl = '/resources/signs';
    const letterList = getLettersFromText(props.text);

    // map to list of images - sign language translations
    let index = 0;
    const imageList = letterList.map( 
        letter => <li key={index++}><img src={`${imageBaseUrl}/${letter}.png`} alt={letter}/></li>
    );

    return (
        <form>
            <div className="display-container">
                <h3 className="original-text"> { props.text } </h3>
                <ul className="sign-list">{ imageList }</ul>
            </div>
        </form>
    )

};

export default TranslationDisplay;
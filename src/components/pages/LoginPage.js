import React from 'react';
import InputForm from '../forms/InputForm';
import { setStorage } from '../../utils/storage';
import { useHistory } from 'react-router-dom';

const LoginPage = () => {

    const history = useHistory();

    const handleLoginClicked = (inputValue) => {
        // store name entered as session info
        setStorage('session_info', {
            name: inputValue,
            translations: []
        });
        // redirect to translation
        history.replace("/translation");
    }

    return (
        <div>
            <div className="container">
                <InputForm placeholderString={"Enter your name"} 
                buttonText ={"Submit"}
                click={handleLoginClicked} />
                <div className="decoration"></div>
            </div>
        </div>
    )

};

export default LoginPage;
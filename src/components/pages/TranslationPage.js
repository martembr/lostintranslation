import React, { useState } from 'react';
import TranslationDisplay from '../display/TranslationDisplay';
import InputForm from '../forms/InputForm';
import { addTranslation } from '../../utils/manageTranslationStorage';

const TranslationPage = () => {

    const [ textToTranslate, setTextToTranslate ] = useState('');

    const handleClicked = (inputValue) => {
        // add translation to storage 
        addTranslation(inputValue);
        // set textToTranslate
        setTextToTranslate(inputValue);
    }

    return (
        <div>
            <InputForm placeholderString={"Enter text to translate"} 
                buttonText = {"Translate"}
                click={handleClicked} />
            <div className="container">
                <TranslationDisplay text={ textToTranslate }/>
                <div className="decoration"></div>
            </div>
            
        </div>
    )

};

export default TranslationPage;
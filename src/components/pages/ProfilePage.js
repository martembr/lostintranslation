import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { clearStorage } from '../../utils/storage';
import { clearTranslations, getTranslations } from '../../utils/manageTranslationStorage';
import TranslationListDisplay from '../display/TranslationListDisplay';
import '../styles/profile.css';

const ProfilePage = () => {

    const history = useHistory();
    const [ translations, setTranslations ] = useState([]); 


    useEffect(() => {
        setTranslations(getTranslations());
    }, []);


    const onLogout = () => {
        // clear storage
        clearStorage('session_info');
        // redirect to login
        history.replace("/login");
    }


    const onClearTranslations = () => {
        // update translations variable 
        setTranslations([]);
        // clear from storage
        clearTranslations();
    }


    return (
        <div className="profile-container">
            <button className="logout-btn" type="button" onClick = { onLogout }>Log out</button>
            <h3 className="profile-header">Your previous translations:</h3>
            <TranslationListDisplay translations = {translations} />
            <button className="clear-btn" type="button" onClick = { onClearTranslations }>Clear translations</button>
        </div>
    )

};

export default ProfilePage;
import React  from 'react';
import '../styles/header.css'

function Header() {

    return (
        <header>
            <div className="header-container">
                <img className="logo-img" src="resources/Logo.png" alt="logo"/>
                <h1 className="header-text">Lost in translation</h1>
            </div>
        </header>
    )

};

export default Header;
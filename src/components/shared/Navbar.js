import React from 'react';
import { NavLink} from 'react-router-dom';
import '../styles/navbar.css';

function Navbar() {

    const navItemStyle = {
        fontWeight: 'bold'
    };

    return (
        <nav className="main-navigation">
            <NavLink to="/login"  className="nav-item" activeStyle = {navItemStyle}> Login </NavLink>
            |
            <NavLink to="/translation" className="nav-item" activeStyle = {navItemStyle}> Translation </NavLink>
            | 
            <NavLink to="/profile" className="nav-item" activeStyle = {navItemStyle}> Profile </NavLink>
        </nav>
    );
}

export default Navbar;
import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import LoginPage from './components/pages/LoginPage';
import ProfilePage from './components/pages/ProfilePage';
import TranslationPage from './components/pages/TranslationPage';
import NotFoundPage from './components/pages/NotFoundPage';
import Header from './components/shared/Header';
import Navbar from './components/shared/Navbar';

function App() {
  return (
    <Router>

      <div className="App">

        <Header/>
        <Navbar/>

         <Switch>
          <Route path="/login" component = { LoginPage }/>
          <Route path="/translation" component = { TranslationPage }/>
          <Route path="/profile" component = { ProfilePage }/>
          <Route exact path="/">
            <Redirect to="/login" />
          </Route> 
          <Route path="*" component = { NotFoundPage }/>
        </Switch>

      </div>
      
    </Router>
  );
}

export default App;
